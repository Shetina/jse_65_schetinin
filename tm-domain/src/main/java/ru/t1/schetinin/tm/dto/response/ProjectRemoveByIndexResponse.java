package ru.t1.schetinin.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectRemoveByIndexResponse extends AbstractProjectResponse {

    public ProjectRemoveByIndexResponse(@NotNull final ProjectDTO project) {
        super(project);
    }

}