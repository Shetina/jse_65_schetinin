package ru.t1.schetinin.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.schetinin.tm")
public class ApplicationConfiguration {

}