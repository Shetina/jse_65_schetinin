package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.Project;

import java.util.List;

public interface IProjectRestEndpoint {

    @Nullable
    List<Project> findAll() throws Exception;

    @NotNull
    Project save(@NotNull Project project) throws Exception;

    @Nullable
    Project findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull Project project) throws Exception;

    void clear() throws Exception;

}
