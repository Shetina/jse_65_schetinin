package ru.t1.schetinin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskRestEndpoint {

    @Nullable
    List<Task> findAll() throws Exception;

    @NotNull
    Task save(@NotNull Task project) throws Exception;

    @Nullable
    Task findById(@NotNull String id) throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    long count() throws Exception;

    void deleteById(@NotNull String id) throws Exception;

    void delete(@NotNull Task project) throws Exception;

    void clear() throws Exception;

}
