package ru.t1.schetinin.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.api.service.IProjectService;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.field.StatusEmptyException;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository repository;

    @Override
    @Nullable
    public List<Project> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @NotNull
    @Transactional
    public Project add(@NotNull final Project model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<Project> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@NotNull final Project model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public Project update(@NotNull final Project model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(@Nullable final String id, @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @NotNull
    @Transactional
    public Project create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        return repository.save(project);
    }

    @Override
    @NotNull
    @Transactional
    public Project create(@Nullable final String name, @Nullable final String description) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

}
