package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.schetinin.tm.api.service.IProjectService;
import ru.t1.schetinin.tm.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() throws Exception {
        return projectService.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(@RequestBody final @NotNull Project project) throws Exception {
        return projectService.update(project);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") final @NotNull String id) throws Exception {
        return projectService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) throws Exception {
        return (projectService.findOneById(id) == null);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull String id) throws Exception {
        projectService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull Project project) throws Exception {
        projectService.remove(project);
    }

    @Override
    public void clear() throws Exception {
        projectService.clear();
    }


}