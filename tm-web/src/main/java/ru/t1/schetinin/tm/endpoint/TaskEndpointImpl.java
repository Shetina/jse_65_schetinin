package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.schetinin.tm.api.endpoint.ITaskRestEndpoint;
import ru.t1.schetinin.tm.api.service.ITaskService;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskEndpointImpl implements ITaskRestEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/findAll")
    public List<Task> findAll() throws Exception {
        return taskService.findAll();
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Task save(@RequestBody final @NotNull Task task) throws Exception {
        return taskService.update(task);
    }

    @Nullable
    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") final @NotNull String id) throws Exception {
        return taskService.findOneById(id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") final @NotNull String id) throws Exception {
        return (taskService.findOneById(id) == null);
    }

    @Override
    @GetMapping("/count")
    public long count() throws Exception {
        return taskService.count();
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull String id) throws Exception {
        taskService.removeById(id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull Task task) throws Exception {
        taskService.remove(task);
    }

    @Override
    public void clear() throws Exception {
        taskService.clear();
    }

}