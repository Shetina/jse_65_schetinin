package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    List<Task> findAll() throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    Task add(@NotNull Task model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    Task findOneById(@NotNull String id) throws Exception;

    void remove(@NotNull Task model) throws Exception;

    Task update(@NotNull Task model) throws Exception;


    int count() throws Exception;

    void changeTaskStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Task create(@Nullable String name) throws Exception;

    @NotNull
    Task create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@Nullable String projectId) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception;

}