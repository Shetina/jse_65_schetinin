package ru.t1.schetinin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.Project;

import java.util.List;

public interface IProjectService {

    @Nullable
    List<Project> findAll() throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    Project add(@NotNull Project model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    Project findOneById(@NotNull String id) throws Exception;

    void remove(@NotNull Project model) throws Exception;

    Project update(@NotNull Project model) throws Exception;

    void changeProjectStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    Project create(@Nullable String name) throws Exception;

    @NotNull
    Project create(@Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    int count() throws Exception;

}
