package ru.t1.schetinin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.schetinin.tm.event.ConsoleEvent;
import ru.t1.schetinin.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectListener extends AbstractTaskListener {

    @NotNull
    private static final String NAME = "task-bind-to-project";

    @NotNull
    private static final String DESCRIPTION = "Bind task to project";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskBindToProjectListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        taskEndpoint.bindTaskToProject(request);
    }

}