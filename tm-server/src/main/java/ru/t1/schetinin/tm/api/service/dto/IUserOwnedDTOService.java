package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.schetinin.tm.enumerated.TMSort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable TMSort sort) throws Exception;

    void removeById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws Exception;

    void clear(@NotNull String userId) throws Exception;

    boolean existsById(@NotNull String userId, @NotNull String id) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id) throws Exception;

    int getSize(@NotNull String userId) throws Exception;

    void remove(@NotNull String userId, @NotNull M model) throws Exception;

    void update(@NotNull String userId, @NotNull M model) throws Exception;

}
