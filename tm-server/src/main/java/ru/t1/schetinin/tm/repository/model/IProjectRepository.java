package ru.t1.schetinin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    long countByUser(@NotNull final User user);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Project> findByUser(@NotNull final User user);

    @NotNull
    List<Project> findByUser(@NotNull final User user, @NotNull final Sort sort);

    @NotNull
    Optional<Project> findByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}